package com.springsource.roo.location.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Location {

    /**
     */
    private String devicetype;

    /**
     */
    private Double longitude;

    /**
     */
    private Double latitude;

    /**
     */
    private long locationID;
}
